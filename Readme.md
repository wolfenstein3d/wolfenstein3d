# Wolfenstein3D

A repository for the launch script and compiled binaries which is part of the AUR packages

https://aur.archlinux.org/packages/wolf3d
https://aur.archlinux.org/packages/wolf4sdl-bin

The repositories,compiled binaries and packages are not associated with the [wolf4sdl](https://github.com/lazd/wolf4sdl) project or id Software and all rights go to id Software.

Please support the wolf4sdl project as this project wouldn't exist without it.
